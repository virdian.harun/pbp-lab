from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm

# Create your views here.
@login_required(login_url='/admin/login')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

def add_friend(request):
    form = FriendForm(request.POST)
    if form.is_valid() and request.method == 'POST':
        form.save()
        if "add_another" in request.POST:
            messages.success(request, 'Friend successfully added!')
            return HttpResponseRedirect(request.path_info)
        else:
            return HttpResponseRedirect("/lab-3")
        
    context = {
        "form" : form,
    }
    return render(request, 'lab3_form.html', context)