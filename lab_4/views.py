from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
@login_required(login_url='/admin/login')
def index(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST)
    if form.is_valid() and request.method == 'POST':
        form.save()
        if "add_another" in request.POST:
            messages.success(request, 'Note successfully added!')
            return HttpResponseRedirect(request.path_info)
        else:
            return HttpResponseRedirect("/lab-4")
    else:
        form = NoteForm()
        
    context = {
        "form" : form,
    }
    return render(request, 'lab4_form.html', context)

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes' : notes}
    return render(request, 'lab4_note-list.html', response)