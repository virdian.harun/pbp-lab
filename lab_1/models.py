from django.db import models

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.DecimalField(max_digits=10, decimal_places=0)
    dob = models.DateField()
