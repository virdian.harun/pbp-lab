1. Apakah perbedaan antara JSON dan XML? <br>
XML:
    - Merupakan sebuah markup language, berorientasi pada dokumen
    - Mendukung data dalam bentuk teks, angka, gambar, grafik, dan sebagainya
    - Data disimpan dalam bentuk tree
    - Mendukung penggunaan comment, berbagai macam encoding.
    - Tidak mendukung penggunaan array
    - Menggunakan tag awal dan akhir

    JSON:
    - Merupakan cara merepresentasikan objek, berorientasi pada data
    - Hanya mendukung data dalam bentuk teks dan angka
    - Data disimpan dalam bentuk map yang memiliki key dan value
    - Mendukung penggunaan array dan encoding UTF-8.
    - Tidak mendukung penggunaan comment
    - Tidak menggunakan tag




2. Apakah perbedaan antara HTML dan XML? <br>
HTML:
    - Digunakan untuk menampilkan data
    - Tagnya sudah ditentukan dan berfungsi untuk menampilkan data
    - Tidak peka huruf besar/kecil
    - Tidak harus menggunakan tag penutup
    - Tidak mempertahankan whitespaces


    XML:
    - Digunakan untuk menyimpan dan mentransfer data
    - Tagnya didefinisikan oleh pengguna dan berfungsi untuk mendeskripsikan data
    - Peka huruf besar/kecil
    - Harus menggunakan tag penutup
    - Dapat mempertahankan whitespaces



