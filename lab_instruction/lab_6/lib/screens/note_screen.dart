import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class NoteScreen extends StatelessWidget {
  static const routeName = '/note';

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title:  Text("NOTE")
      ), 

      endDrawer: MainDrawer(),
      body: Center(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(20, 30, 20, 40),
                child: Text(
                "My Quick Note",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 36,
                )
              )
            ), 
            SizedBox(
              height: 400,
              width: 460,
              child: Card (
                color: Color.fromRGBO(41, 43, 50, 1),
                child: Padding(
                  padding: EdgeInsets.fromLTRB(14, 8, 12, 0),
                  child: SingleChildScrollView(
                    child: TextField(
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      cursorColor: Colors.white,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                      ),
                      style: TextStyle(
                        fontSize: 16,
                        height: 1.5,
                        color: Colors.white,
                      )
                    ),
                  )
                )
              )
            )
          ]
        )
      ),
        
      floatingActionButton: FloatingActionButton(
        onPressed: () => test(),
        child: Icon(
          Icons.edit,
          color: Colors.white),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        foregroundColor: Color.fromRGBO(27, 29, 36, 1),
      )
    );
  }
}

void test() {
  readOnly = !readOnly;
}