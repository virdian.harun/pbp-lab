import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class PlannerScreen extends StatelessWidget {
  static const routeName = '/planner';

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title:  Text("PLANNER")
      ), 

      endDrawer: MainDrawer(),
      body: Center(
        child: SingleChildScrollView (
          child : Column(
            children: [
              Center(
                child: Container(
                  padding: EdgeInsets.all(20.0),
                  child: Text(
                    "My Planner",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 36,
                    )
                  )
                )
              ),   
              dayTitle("Monday"),
              plannerCard("Tester1", TimeOfDay.now(), TimeOfDay.now()),
              dayTitle("Tuesday"),
              dayTitle("Wednesday"),
              plannerCard("Tester2", TimeOfDay.now(), TimeOfDay.now()),
              plannerCard("Tester3", TimeOfDay.now(), TimeOfDay.now()),
              dayTitle("Thursday"),
              dayTitle("Friday"),
              dayTitle("Saturday"),
              plannerCard("Tester4", TimeOfDay.now(), TimeOfDay.now()),
              dayTitle("Sunday")    
            ]
          )
        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => test(),
        child: const Icon(
          Icons.add,
          color: Colors.white),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10)
        ),
        foregroundColor: Color.fromRGBO(27, 29, 36, 1),
      )
    );
  }
}


  

Widget plannerCard(String title, TimeOfDay start, TimeOfDay end) {
  var startTime = start.toString().substring(10, 15);
  var endTime = end.toString().substring(10, 15);
  var time = "$startTime - $endTime";

  return Container(
    padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
    child: SizedBox(
      height: 160,
      width: 270,
      child: Card(
        color: Color.fromRGBO(33, 125, 187, 1),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
              child: 
              Text(
                title,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 18
                )
              )
            ),
            Divider(
              color: Colors.white,
              thickness:  3,
              indent: 20,
              endIndent: 20,
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: Text(
                time,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16)
              )
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextButton(
                    child: Text(
                      "Edit",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16)
                      ),
                    onPressed: () => test(),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Color.fromRGBO(117, 80, 123, 1))
                    )
                  )
                ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: TextButton(
                    child: Text("Delete",
                      style: TextStyle(
                      color: Colors.white,
                      fontSize: 16)
                    ),
                    onPressed: () => test(),
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Color.fromRGBO(204, 36, 29, 1))
                    )
                  )
                )
              ]
            )
          ]
        )
      )
    )
  );
}

Widget dayTitle(String title) {
  return Container(
    padding: EdgeInsets.all(20),
    child: Text(
      title,
      style: TextStyle(
        fontSize: 30,
        decoration: TextDecoration.underline,
      ))
  );
}

void test() {
}