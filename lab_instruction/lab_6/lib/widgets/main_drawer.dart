import 'package:flutter/material.dart';

import '../screens/note_screen.dart';
import '../screens/planner_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, Function tapHandler) {
    return ListTile(
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          buildListTile('Note', () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Planner', () {
            Navigator.of(context).pushReplacementNamed(PlannerScreen.routeName);
          }),
        ],
      ),
    );
  }
}
